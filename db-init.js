const r = require("./src/rethink");
const minio = require("./src/minio");
const env = require("./src/env");

(async function() {
  try {
    await r.connWaiter;
  } catch (err) {
    console.error(err);
    throw err;
  }
  console.log("CONNECTED");
  try {
    await r.dbCreate(env.rethink.db).run(r.conn);
  } catch (err) {
    console.error(err);
  }
  console.log("DB CREATED");
  try {
    await r.tableCreate("users").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("roles").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("role_members").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("guilds").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("styles").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("members").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("channels").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("invites").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("messages").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("attachments").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("devices").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("games").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  try {
    await r.tableCreate("embeds").run(r.conn);
  } catch (err) {
    console.error(err);
  }
  console.log("TABLES CREATED");

  // The primary key on these is actually
  // `${userId}-${guildId}`, for the sake of
  // cheap lookups and a unique key
  try {
    await r
      .table("members")
      .indexCreate("guildId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  try {
    await r.table("members").indexCreate("guildId");
  } catch (err) {
    console.error(err);
  }

  try {
    await r
      .table("members")
      .indexCreate("userId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  try {
    await r
      .table("role_members")
      .indexCreate("memberId_guildId", [r.row("memberId"), r.row("guildId")])
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  try {
    await r
      .table("role_members")
      .indexCreate("roleId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  try {
    await r
      .table("roles")
      .indexCreate("guildId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  try {
    await r
      .table("roles")
      .indexCreate("position_guildId", [r.row("position"), r.row("guildId")])
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  try {
    await r
      .table("roles")
      .indexCreate("position")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // Get attachments on message id
  try {
    await r
      .table("attachments")
      .indexCreate("messageId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // Get guild channel list
  try {
    await r
      .table("channels")
      .indexCreate("guildId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // Get all messages in channel
  try {
    await r
      .table("messages")
      .indexCreate("channelId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // Adjusting role positions
  try {
    await r
      .table("members")
      .indexCreate("position_guildId", [r.row("position"), r.row("guildId")])
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // GET /channels/:id/messages
  try {
    await r
      .table("messages")
      .indexCreate("channelId_id", [r.row("channelId"), r.row("id")])
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // For sorting purposes, as rethink doesn't let
  // us treat 'id' as a bigint when sorting, thus
  // ID 234 would be treated as newer than 12345...
  try {
    await r
      .table("messages")
      .indexCreate("createdAt")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // For logins
  try {
    await r
      .table("users")
      .indexCreate("email")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // Friend requests / Member list sort
  try {
    await r
      .table("users")
      .indexCreate("username")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // /push/devices
  try {
    await r
      .table("devices")
      .indexCreate("ownerId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  // lookup embeds attached a message
  try {
    await r
      .table("embeds")
      .indexCreate("messageId")
      .run(r.conn);
  } catch (err) {
    console.error(err);
  }

  console.log("INDICES CREATED");

  try {
    await minio.makeBucket(env.minio.bucketPrefix + "-message-attachments");
  } catch (err) {
    console.error(err);
  }

  try {
    await minio.makeBucket(env.minio.bucketPrefix + "-guild-icons");
  } catch (err) {
    console.error(err);
  }

  try {
    await minio.makeBucket(env.minio.bucketPrefix + "-user-avatars");
  } catch (err) {
    console.error(err);
  }

  try {
    await minio.makeBucket(env.minio.bucketPrefix + "-game-assets");
  } catch (err) {
    console.error(err);
  }

  console.log("S3 BUCKET CREATED");

  console.log("INIT'd - Database initialised with tables and indices");
  return true;
})().catch(err => {
  console.error(err);
});
