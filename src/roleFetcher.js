const r = require("./rethink");
const permissionCheck = require("./permissionCheck");

module.exports = async function(req, res, next) {
  const guildId = req.channel ? req.channel.guildId : req.guild.id;

  const roleCursor = await r
    .table("role_members")
    .getAll([req.user.id, guildId], {
      index: "memberId_guildId"
    })
    .eqJoin("roleId", r.table("roles"))
    .run(r.conn);
  const roles = [];
  try {
    while ((nextRole = await roleCursor.next())) {
      roles.push(nextRole.right);
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }
  const guildRole = await r
    .table("roles")
    .get(guildId)
    .run(r.conn);

  // A little bit faster
  let totalPermissions = guildRole.permissions;

  for (const role of roles) {
    totalPermissions |= role.permissions;
  }

  if (req.channel) {
    // TODO: Permission overwrites
  }

  console.log(req.guild, req.channel, req.path);

  if (req.guild.ownerId == req.user.id) {
    totalPermissions |= permissionCheck.permissions.ADMIN;
  }

  roles.push(guildRole);
  req.roles = roles;
  req.totalPermissions = totalPermissions;

  next();
};
