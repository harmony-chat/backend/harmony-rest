const Minio = require("minio");
const env = require("./env");

const minio = new Minio.Client({
  endPoint: env.minio.host,
  port: env.minio.port,
  accessKey: env.minio.access,
  secretKey: env.minio.secret,
  useSSL: env.minio.useSSL
});

module.exports = minio;
