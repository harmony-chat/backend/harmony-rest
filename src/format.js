module.exports = {
  message: m => ({
    id: m.id,
    content: m.content,
    authorId: m.authorId,
    channelId: m.channelId,
    attachments: m.attachments && m.attachments.map(module.exports.attachment),
    gameId: m.gameId,
    gameOptions: m.gameOptions,
    embeds: m.embeds && m.embeds.map(module.exports.embed)
  }),
  guild: g => ({
    id: g.id,
    name: g.name,
    icon: g.icon,
    ownerId: g.ownerId
  }),
  channel: c => ({
    id: c.id,
    name: c.name,
    guildId: c.guildId,
    media: c.media
  }),
  member: u => ({
    id: u.id,
    guildId: u.guildId,
    nickname: u.nickname
  }),
  user: u => ({
    id: u.id,
    username: u.username,
    avatar: u.avatar,
    activity: u.activity,
    presence: u.presence
  }),
  invite: (g, i) => ({
    id: i.id,
    name: g.name,
    guildId: i.guildId,

    channelId: i.channelId
  }),
  attachment: a => ({
    id: a.id,
    width: a.width,
    height: a.height,
    mime: a.mime,
    filename: a.filename,
    size: a.size,
    uploaded: a.uploaded
  }),
  role: r => ({
    id: r.id,
    guildId: r.guildId,
    name: r.name,
    permissions: r.permissions,
    position: r.position
  }),
  device: d => ({
    id: d.id,
    enabled: d.enabled
  }),
  game: g => ({
    id: g.id,
    name: g.name,
    description: g.description,
    code: g.code
  }),
  embed: e => ({
    id: e.id,
    type: e.type,
    title: e.title,
    link: e.link,
    description: e.description,
    player: e.player,
    image: e.image,
    height: e.height,
    width: e.width,
    color: e.color
  })
};
