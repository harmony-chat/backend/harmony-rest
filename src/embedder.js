const snekfetch = require("snekfetch");
const env = require("./env");

const linkRegex = /(https?:\/\/[^\s<]+[^<.,:;"')\]\s])/g;

// /<([^ >]+:\/[^ >]+)>/g;

module.exports = {
  links(content) {
    const links = content.match(linkRegex);
    console.log("links", links);
    return links || [];
  },
  async scrapeAndDispatch(data) {
    console.log("Scraping");
    const res = await snekfetch.post(env.embedder + "/embed").send(data);
    console.log("Scraped", res.body);
  }
};
