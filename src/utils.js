const Flake = require("flake-idgen");

const snekfetch = require("snekfetch");
const env = require("./env");
const flake = new Flake({
  epoch: env.epoch
});
const bigNumber = require("bignum");
const mongo = require("./mongo");

function generateSnowflake() {
  return new Promise(function(resolve, reject) {
    flake.next(function(err, id) {
      if (err) reject(err);
      else resolve(bigNumber.fromBuffer(id).toString());
    });
  });
}

const ALL_KEYS = [["c", "channelId"], ["g", "guildId"], ["u", "userId"]];
const ALL_KEYS_MAP = new Map(ALL_KEYS);

async function dispatch(eventName, data, metadata) {
  if (metadata) {
    console.log("METAMETAMETA", metadata);
  }
  const underIndex = eventName.indexOf("_");
  const eventAction = eventName.substring(underIndex + 1);
  const eventType = eventName.substring(0, underIndex);

  let itemId = null;
  let key = null;

  for (const [subscriptionKey, eventKey] of ALL_KEYS) {
    if (data[eventKey]) {
      itemId = data[eventKey];
      key = subscriptionKey;
      break;
    }
  }

  const updateFlag = {
    $set: {
      dispatchId: await module.exports.generateSnowflake(),
      itemId,
      eventType,
      eventAction
    }
  };

  if (eventAction == "DELETE") {
    updateFlag.$set.payload = {
      id: itemId
    };
  } else {
    for (const key in data) {
      updateFlag.$set[`payload.${key}`] = data[key];
    }
  }

  console.log("PRomises");
  await Promise.all([
    mongo.db.collection("events").updateOne(
      {
        subscriptionQuery: key + "_" + itemId,
        eventType,
        itemId
      },
      updateFlag,
      {
        upsert: true
      }
    ),
    mongo.db
      .collection("subscriptions")
      .findAsync({
        subscriptionQuery: key + "_" + itemId,
        active: true
      })
      .then(async subscriptions => {
        const hostAddresses = new Map();
        const currentSubscriptions = new Set();
        for await (const subscription of subscriptions) {
          const existing = hostAddresses.get(subscription.hostAddress);
          if (existing) existing.add(subscription.sessionId);
          else
            hostAddresses.set(
              subscription.hostAddress,
              new Set([subscription.sessionId])
            );
          currentSubscriptions.add(subscription);
        }

        const promises = new Set();

        if (metadata) {
          promises.add(
            metadata
              .eligibleUsers({
                pushBody: metadata.pushBody,
                currentSubscriptions
              })
              .then(async users => {
                if (users) {
                  const notifPromises = new Set();
                  for await (const userId of users) {
                    notifPromises.add(
                      notifier.sendNotification(userId, metadata.pushBody)
                    );
                  }
                  return await Promise.all(notifPromises.values());
                }
              })
          );
        }

        for (const [gateway, sessions] of hostAddresses.entries()) {
          console.log("Dispatching message to gw @", gateway);
          promises.add(
            snekfetch.post(gateway).send({
              d: [
                {
                  t: eventName,
                  d: data
                }
              ],
              r: Array.from(sessions)
            })
          );
        }

        return await Promise.all(promises.values());
      })
  ]);
}

const nodePath = require("path");

// This is such shitcode but it works
// TODO: get a better fucking framework (polka?)
function discoverRoutes(path, router, base) {
  for (const layer of router.stack) {
    if (layer.route && path.match(layer.regexp)) {
      return nodePath.resolve(base + layer.route.path);
      // fullRoutes.add(nodePath.resolve(base + layer.route.path));
    }

    if (layer.handle && layer.handle.stack) {
      const exec = layer.regexp.exec(path);
      if (exec) {
        return discoverRoutes(
          // The existing path minus the router's base
          path.substring(exec.index + exec[0].length),
          layer.handle,
          // fullRoutes,
          // TODO: this is slow... we can do better
          exec[0].replace(layer.regexp, function(orig, ...keys) {
            for (const keyId in keys) {
              if (layer.keys[keyId]) {
                // Inject params like :channelId
                orig = orig.replace(keys[keyId], ":" + layer.keys[keyId].name);
              }
            }
            return base + orig;
          })
        );
      }
    }
  }
}

const yup = require("yup");

require("./schema/_types/index.js");

async function validateSchema(router, req, res, next) {
  const route = discoverRoutes(req.path, router, "");
  try {
    const schema = require(`./schema/${route}/${req.method}.js`);

    if (!schema.description) throw new Error("No description for request!");
    if (!schema.response) throw new Error("No schema for response!");

    if (schema.request) {
      console.log("HAS SCHEMA!");
      if (!req.body) {
        console.log("No body to validate against!");
        return res.status(422).json({
          errors: ["No body provided!"],
          path: ""
        });
      }
      try {
        await schema.request.validate(req.body);
      } catch (err) {
        console.log("invalid!", err);
        return res.status(422).json({
          errors: err.errors,
          path: err.path
        });
      }
    }

    if (env.deployment != "production") {
      res._json = res.json;
      res.json = async function(body) {
        if (res.statusCode === 200 || res.statusCode === null) {
          try {
            await schema.response.validate(body);
          } catch (err) {
            console.error(
              `Response doesn't match schema for ${req.method} ${route}!`,
              body,
              err
            );
          }
          res._json(body);
        }
      };
    }
  } catch (err) {
    if (err.code == "MODULE_NOT_FOUND")
      console.log(`[NOT FATAL] — Schema for ${req.method} ${route} not found!`);
    else
      console.error(`Error loading schema for ${req.method} ${req.path}!`, err);
  }
  next();
}

function cursorIterator(cursor) {
  return {
    [Symbol.asyncIterator]() {
      return {
        cursor,
        async next() {
          try {
            return {
              value: await this.cursor.next().catch(err => Promise.reject(err)),
              done: false
            };
          } catch (err) {
            console.log(
              err.name,
              err.name == "ReqlDriverError",
              err.message,
              err.message == "No more rows in the cursor."
            );
            if (
              err.name == "ReqlDriverError" &&
              err.message == "No more rows in the cursor."
            ) {
              console.log("Caught!");
              return {done: true};
            } else {
              console.log("Throwing", err);
              throw err;
            }
          }
        }
      };
    }
  };
}

module.exports = {
  generateSnowflake,
  dispatch,
  validateSchema,
  cursorIterator
};

const notifier = require("./notifier");
