const env = require("./env");
const crypto = require("crypto");

function generateMagic(userId, secret, justBuffer) {
  function signature(key, data) {
    const hmacAlgo = crypto.createHmac("sha1", key);
    hmacAlgo.update(data);
    return hmacAlgo.digest("binary");
  }

  const buffer = Buffer.from(
    signature(signature(env.serverSalt, secret), userId)
  );
  if (justBuffer) return buffer;
  else return buffer.toString("base64");
}

function generateToken(userId, secret) {
  const time = Buffer.from(
    (new Date().getTime() - env.epoch).toString(16),
    "hex"
  ).toString("base64");
  return userId + "." + time + "." + generateMagic(userId, secret);
}

function checkLogin(token, secret, userId) {
  const lastDotIndex = token.lastIndexOf(".");
  const magic = token.substring(lastDotIndex + 1);
  try {
    var oldBuffer = Buffer.from(magic, "base64");
  } catch (err) {
    // Basically just means it's not b64'd
    return false;
  }
  const newBuffer = generateMagic(userId, secret, true);
  if (oldBuffer.length != newBuffer.length) return false;

  return crypto.timingSafeEqual(oldBuffer, newBuffer);
}

module.exports = {
  generateMagic,
  generateToken,
  checkLogin
};
