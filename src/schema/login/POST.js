const yup = require("yup");

exports.description =
  "Gets a token of the user with the given login credentials";

exports.request = yup.object().shape({
  email: yup.string().email(),
  password: yup
    .string()
    .min(8)
    .max(100)
});

exports.response = yup.object().shape({
  token: yup.string()
});
