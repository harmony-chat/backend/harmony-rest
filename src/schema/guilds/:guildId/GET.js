const yup = require("yup");

exports.description = "Returns the specified guild";

exports.response = yup.object().shape({
  id: yup.snowflake(),
  name: yup
    .string()
    .min(2)
    .max(32),
  ownerId: yup.snowflake(),
  icon: yup.string().nullable()
});
