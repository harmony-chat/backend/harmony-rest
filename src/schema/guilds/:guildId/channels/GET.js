const yup = require("yup");

exports.description = "Fetches the list of channels in the guild";

exports.response = yup
  .array()
  .of(require("../../../channels/:channelId/GET").response);
