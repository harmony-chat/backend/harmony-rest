const yup = require("yup");

exports.description = "Creates a new channel in the guild";

exports.request = yup.object().shape({
  name: yup
    .string()
    .min(2)
    .max(32)
});

exports.response = require("../../../channels/:channelId/GET").response;
