const yup = require("yup");

exports.description = "Sends a message to the channel";

exports.request = yup.object().shape({
  content: yup
    .string()
    .notRequired()
    .max(2000),
  gameId: yup.snowflake().notRequired(),
  gameOptions: yup
    .string()
    .notRequired()
    .max(2000),
  nonce: yup.snowflake().notRequired(),
  attachments: yup
    .array()
    .notRequired()
    .of(
      yup.object().shape({
        nonce: yup.snowflake().notRequired(),
        geometry: yup
          .object()
          .notRequired()
          .shape({
            height: yup
              .number()
              .positive()
              .integer(),
            width: yup
              .number()
              .positive()
              .integer()
          }),
        mime: yup.string().matches(/^[-\w]+\/[-\w+]+$/),
        filename: yup.string().required(),
        size: yup
          .number()
          .positive()
          .integer()
      })
    )
    .max(5)
});

exports.response = require("./:messageId/GET").response;
