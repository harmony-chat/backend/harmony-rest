const yup = require("yup");

exports.description = "Fetches a specific message by its id";

exports.response = yup.object().shape({
  id: yup.snowflake(),
  nonce: yup.snowflake().notRequired(),
  content: yup.string().max(2000),
  authorId: yup.snowflake(),
  channelid: yup.snowflake()
});
