const yup = require("yup");

exports.description = "Edits the selected message";

exports.request = require("../POST").request;

exports.response = require("./GET").response;
