const yup = require("yup");

exports.description = "Returns a list of message objects";

exports.search = yup.object().shape({
  before: yup
    .number()
    .default(Infinity)
    .positive()
    .integer(),
  after: yup
    .number()
    .default(1)
    .positive()
    .integer(),
  limit: yup
    .number()
    .default(50)
    .min(1)
    .max(50)
});

exports.response = yup
  .array()
  .min(0)
  .max(50)
  .of(require("./:messageId/GET").response);
