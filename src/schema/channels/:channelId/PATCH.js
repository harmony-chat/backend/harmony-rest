const yup = require("yup");

exports.description = "Edits the requested channel";

exports.response = require("./GET").response;

exports.request = yup.object().shape({
  name: yup
    .string()
    .min(2)
    .max(32)
});
