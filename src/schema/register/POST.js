const yup = require("yup");

exports.description = "Creates a new account";

exports.request = yup.object().shape({
  username: yup.username(),
  email: yup.string().email(),
  password: yup
    .string()
    .min(8)
    .max(100)
});

exports.response = yup.object().shape({
  success: yup.boolean()
});
