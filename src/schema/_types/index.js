const yup = require("yup");

const types = ["snowflake", "username"];

for (const type of types) {
  const Type = require("./" + type);
  yup[type] = () => new Type();
}

module.exports = yup;
