const yup = require("yup");

const StringSchema = yup.string;
const invalid = null;

class SnowflakeSchema extends StringSchema {
  constructor() {
    super();
    this.withMutation(() => {
      this.transform(function(value, originalvalue) {
        if (this.isType(value)) return value;
        else return invalid;
      });
    });
  }

  _typeCheck(value) {
    return super._typeCheck(value) || (value.length && value.match(/^[0-9]+$/));
  }
}

module.exports = SnowflakeSchema;
