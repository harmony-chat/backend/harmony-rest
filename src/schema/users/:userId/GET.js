const yup = require("yup");

exports.description = "Fetches the requested user";

exports.response = yup.object().shape({
  id: yup.snowflake(),
  username: yup.username()
});
