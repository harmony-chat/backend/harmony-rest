const env = require("./env");

const MongoDB = require("mongodb");

const BB = require("bluebird");
BB.promisifyAll(MongoDB);

MongoDB.connectAsync(`mongodb://${env.mongo.host}:${env.mongo.port}`, {
  useNewUrlParser: true
}).then(mongo => {
  module.exports.db = mongo.db(env.mongo.db);
});
