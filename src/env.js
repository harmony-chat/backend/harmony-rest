const permissionCheck = require("./permissionCheck");

module.exports = {
  mongo: {
    db: process.env.MONGO_DB || "harmony-state",
    host: process.env.MONGO_HOST || "localhost",
    port: process.env.MONGO_PORT || "27017"
  },
  vapid: {
    publicKey:
      process.env.VAPID_PUBKEY ||
      "BC5kwKgoeEBL1ona8pBX6ZQjetS9fS3xObhE09SKJjXEXKqs5RYoUwi9T_dj5xpuIYwFV6BLF-VEVJ_Zp9Kb500",
    privateKey:
      process.env.VAPID_PRIVKEY || "CdwcfXT-JF8tTwrcC3xu04JPGU71PuU-fhRx2K9dW0M"
  },

  deployment: process.env.NODE_ENV || "local",
  rethink: {
    host: process.env.RETHINK_HOST || "localhost",
    port: process.env.RETHINK_PORT ? parseInt(process.env.RETHINK_PORT) : 28015,
    user: process.env.RETHINK_USER || "admin",
    password: process.env.RETHINK_PASSWORD || "",
    get db() {
      return `harmony_${module.exports.deployment}`;
    }
  },
  minio: {
    get bucketPrefix() {
      return `harmony-${module.exports.deployment}`;
    },
    host: process.env.MINIO_HOST || "play.minio.io",
    port: process.env.MINIO_PORT ? parseInt(process.env.MINIO_PORT) : 9000,
    access: process.env.MINIO_ACCESS || "Q3AM3UQ867SPQQA43P2F",
    secret:
      process.env.MINIO_SECRET || "zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",
    useSSL: !process.env.MINIO_INSECURE
  },
  serverSalt: process.env.SERVER_SALT || "salt",
  epoch: (process.env.EPOCH
    ? new Date(Date.UTC(process.env.EPOCH))
    : new Date(Date.UTC(2018, 1, 1))
  ).getTime(),
  gateway: "ws://localhost:3005",
  embedder:
    "http://" +
    (process.env.EMBEDDER_HOST || "localhost") +
    ":" +
    (process.env.EMBEDDER_PORT || "3021"),
  defaultPermissionNames: [
    "INVITE",
    "VIEW_CHANNEL",
    "MESSAGE_CREATE",
    "SET_NICKNAME"
  ],
  get defaultPermissions() {
    let perms = 0;
    for (const permissionName of module.exports.defaultPermissionNames) {
      perms |= permissionCheck.permissions[permissionName];
    }
    return perms;
  }
};
