const webPush = require("web-push");
const r = require("./rethink");
const env = require("./env");
const utils = require("./utils");

module.exports = {
  async sendNotification(user, data) {
    const payload = JSON.stringify({data});
    console.log("Sending off payload..", payload, user);
    const deviceCursor = await r
      .table("devices")
      .getAll(user, {index: "ownerId"})
      .filter({enabled: true})
      .run(r.conn);

    console.log(utils.cursorIterator);
    const devices = utils.cursorIterator(deviceCursor);
    const promises = new Set();
    for await (const device of devices) {
      console.log("Posting to device", device.id);
      promises.add(
        webPush
          .sendNotification(device.subscriptionData, payload, {
            vapidDetails: {
              publicKey: env.vapid.publicKey,
              privateKey: env.vapid.privateKey,
              subject: "https://gitlab.com/harmony-chat/backend/harmony-rest"
            }
          })
          .catch(err => console.error(err))
      );
    }
    return await Promise.all(promises);
  }
};
