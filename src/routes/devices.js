const router = require("express").Router();
const format = require("../format");

router.get("/", async function(req, res) {
  res.json(format.device(req.device));
});

module.exports = router;
