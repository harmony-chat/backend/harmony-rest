const router = require("express").Router();
const env = require("../env");
const r = require("../rethink");
const {generateSnowflake} = (utils = require("../utils"));
const format = require("../format");
const permissionCheck = require("../permissionCheck");
const messages = require("./messages");
const bigNumber = require("bignum");
const embedder = require("../embedder");
const constants = require("../constants");

router.get("/", function(req, res) {
  res.json(format.channel(req.channel));
});

async function eligibleUsers(guildId, list, blacklist) {
  const allUsers = await r
    .table("members")
    .getAll(guildId, {index: "guildId"})
    .eqJoin("userId", r.table("users"))
    .zip()
    .filter(function(doc) {
      const userFilter = r.expr(list).contains(doc("userId"));
      return r.and([
        doc("presence").ne(constants.presence.dnd),
        blacklist ? userFilter.not() : userFilter
      ]);
    })
    .map(function(doc) {
      return doc("userId");
    })
    .run(r.conn);
  console.log(await allUsers.toArray(), "allUsers", list, blacklist);
  return utils.cursorIterator(allUsers);
}

async function sortMemberships(members, roleMap) {
  console.log("sortMemberships()", members, roleMap);
  return await Promise.all(
    members.map(async member => {
      console.log("sortMemberships map()", member.userId);
      if (member.nickname)
        return {sorting: member.nickname, value: member.userId};
      else
        return {
          sorting: await r
            .table("users")
            .get(member.userId)
            .do(user => user("username"))
            .run(r.conn),
          value: member.userId
        };
    })
  )
    .then(members => {
      console.log("Got mmebers to sort..", members);
      return members
        .sort((a, b) => {
          const bigA = a.sorting.toUpperCase();
          const bigB = b.sorting.toUpperCase();
          const sorting = bigA < bigB ? -1 : bigA > bigB ? 1 : 0;
          console.log("SORTING....", sorting, bigA, bigB);
          return sorting;
        })
        .sort((a, b) => a.sorting - b.sorting)
        .map(({value}) => value);
    })
    .then(value => {
      console.log("Finally() resolved a sortMemberships():", value);
      return value;
    });
}

router.get("/members", async function(req, res) {
  let limit = 50;
  if (req.query.limit) {
    const numLimit = Number(req.query.limit);
    if (numLimit < 50 && numLimit >= 1) limit = numLimit;
  }

  const [roles, members] = await Promise.all([
    r
      .table("roles")
      .getAll(req.guild.id, {index: "guildId"})
      .orderBy(r.asc("position"))
      .pluck(["position", "permissions", "id"])
      .run(r.conn)
      .then(cursor => cursor.toArray()),
    r
      .table("members")
      .getAll(req.guild.id, {index: "guildId"})
      // Imperfect, sadly we need to sort by username as well but for now it works I guess
      // .orderBy(r.asc("nickname"))
      .run(r.conn)
      .then(cursor => cursor.toArray())
  ]);
  const resulting = [];
  const userMap = new Map();
  const roleMap = new Map(roles.map(role => [role.id, role]));

  const resultingMap = new Map();
  for (const member of members) {
    // Still have to fetch this regardless of guild ownership for hoisting roles
    // (Also TODO: role hoisting stuff)
    const membershipCursor = await r
      .table("role_members")
      .getAll([member.userId, req.guild.id], {index: "memberId_guildId"})
      .run(r.conn);

    const memberships = await membershipCursor.toArray();
    // @everyone role is a weird case
    memberships.push({roleId: req.guild.id});
    console.log(memberships);
    const sortedMemberships = memberships
      .filter(a => roleMap.get(a.roleId))
      .sort(
        (a, b) =>
          roleMap.get(a.roleId).position - roleMap.get(b.roleId).position
      );

    if (
      req.guild.ownerId == member.userId ||
      memberships.some(membership =>
        permissionCheck.rawPermissionCheck(
          roleMap.get(membership.roleId).permissions,
          [permissionCheck.permissions.VIEW_CHANNEL]
        )
      )
    ) {
      console.log(sortedMemberships);
      const selectedMembership = sortedMemberships[0];
      const existingMembership = resultingMap.get(selectedMembership.roleId);
      if (existingMembership) existingMembership.push(member);
      else resultingMap.set(selectedMembership.roleId, [member]);
    }
  }
  let remainingLimit = limit;
  let foundAfter = false;
  for (const role of roles) {
    const roleMembers = resultingMap.get(role.id);
    if (!roleMembers) {
      console.log("No roleMembers?", role.id);
      continue;
    }
    console.log("roleMembers", roleMembers, req.query.after);
    const afterIndex =
      req.query.after && !foundAfter
        ? roleMembers.map(member => member.id).indexOf(req.query.after)
        : 0;
    if (afterIndex === -1 && req.query.after && !foundAfter) {
      continue;
    } else {
      foundAfter = true;
    }
    const sorted = await sortMemberships(roleMembers, roleMap);
    console.log("Sorted...", sorted);
    const sortedMembers = sorted.slice(
      afterIndex === -1 ? 0 : afterIndex,
      remainingLimit
    );
    console.log("SLICING @ 0,", remainingLimit, sortedMembers);
    const sliced = sortedMembers.slice(0, remainingLimit);
    console.log("SLICED...push()ing", sliced, role.id);
    remainingLimit -= sliced.length;
    resulting.push([role.id, sliced]);
    if (remainingLimit <= 0) {
      console.log("ENDING WITH END OF LIMIT");
      break;
    }
  }
  console.log("FINAL!", remainingLimit, resulting);

  res.json(resulting);
});

router.use("/messages", function(req, res, next) {
  if (req.channel.media)
    return res.status(404).json({
      message: "Media channels don't have message"
    });

  next();
});

router.get("/messages", permissionCheck(["VIEW_CHANNEL"]), async function(
  req,
  res
) {
  let limit = 50;
  if (req.query.limit) {
    const numLimit = Number(req.query.limit);
    if (numLimit < 50 && numLimit >= 1) limit = numLimit;
  }
  const messages = await r
    .table("messages")
    .between(req.query.after || r.minval, req.query.before || r.maxval, {
      leftBound: "open",
      index: "id"
    })
    .orderBy({index: r.desc("id")})
    .filter({channelId: req.channel.id})
    .limit(limit)
    .run(r.conn);

  const results = [];
  const messageIds = [];

  for await (const message of utils.cursorIterator(messages)) {
    const messageObj = format.message(message);
    messageObj.embeds = [];
    messageObj.attachments = [];
    results.push(messageObj);
    messageIds.push(message.id);
  }

  if (messageIds.length) {
    await Promise.all([
      r
        .table("attachments")
        .getAll(...messageIds, {index: "messageId"})
        .run(r.conn)
        .then(async attachmentsCursor => {
          for await (const nextAttachment of utils.cursorIterator(
            attachmentsCursor
          )) {
            console.log("GOT ATTACHMENT!");
            const messageIndex = messageIds.indexOf(nextAttachment.messageId);
            results[messageIndex].attachments.push(
              format.attachment(nextAttachment)
            );
          }
        }),

      r
        .table("embeds")
        .getAll(...messageIds, {index: "messageId"})
        .run(r.conn)
        .then(async embedsCursor => {
          for await (const embed of utils.cursorIterator(embedsCursor)) {
            const messageIndex = messageIds.indexOf(embed.messageId);
            results[messageIndex].embeds.push(format.embed(embed));
          }
        })
    ]);
  }

  return res.json(results.reverse());
});

router.post(
  "/messages",
  permissionCheck(["VIEW_CHANNEL", "MESSAGE_CREATE"]),
  async function(req, res) {
    console.log("awoo", req.body);
    const messageId = await generateSnowflake();

    if (req.body.gameId) {
      var game = await r
        .table("games")
        .get(req.body.gameId)
        .run(r.conn);
      if (!game)
        return res
          .status(404)
          .json({message: "The provided game does not exist"});
    }

    const attachments = [];
    if (req.body.attachments) {
      for (const attachment of req.body.attachments) {
        const attachmentObject = {
          id: await generateSnowflake(),
          width:
            (attachment.geometry && attachment.geometry.width) || undefined,
          height:
            (attachment.geometry && attachment.geometry.width) || undefined,
          mime: attachment.mime,
          filename: attachment.filename,
          size: attachment.size,
          uploaded: false,
          messageId
        };
        attachments.push(attachmentObject);
      }
    }

    await r
      .table("attachments")
      .insert(attachments)
      .run(r.conn);

    const messageObject = {
      id: messageId,
      content: String(req.body.content).trim(),
      authorId: req.user.id,
      channelId: req.channel.id,
      createdAt: Date.now() - env.epoch
    };

    if (game) {
      messageObject.gameId = game.id;
      if (req.body.gameOptions)
        messageObject.gameOptions = req.body.gameOptions;
    }

    await r
      .table("messages")
      .insert([messageObject])
      .run(r.conn);

    const links = embedder.links(messageObject.content);

    console.log(links, embedder.scrapeAndDispatch);

    Promise.all(
      links.map(link =>
        embedder.scrapeAndDispatch({
          link,
          messageId: messageObject.id,
          channelId: messageObject.channelId,
          guildId: req.channel.guildId
        })
      )
    )
      .then(() => {
        console.log("Scraped links");
      })
      .catch(err => {
        console.log(err);
      });

    const messageFormat = format.message({
      ...messageObject,
      attachments
    });
    console.log(
      "Dispatching MESSAGE_CREATE with an authorDisplayName...",
      req.member
    );
    const mentionsEveryone =
      messageObject.content.includes("@everyone") &&
      permissionCheck.rawPermissionCheck(
        req.totalPermissions,
        permissionCheck.permissions.MENTION_EVERYONE
      );

    utils.dispatch(
      "MESSAGE_CREATE",
      {
        guildId: req.channel.guildId,
        authorDisplayName: req.member.nickname || req.user.username,
        nonce: req.body.nonce,
        gameIcon: game && game.icon,
        ...messageFormat
      },
      {
        async eligibleUsers({pushBody, currentSubscriptions}) {
          if (pushBody.data.mentionsEveryone) {
            const notMentioningUsers = Array.from(currentSubscriptions)
              .map(sub => sub.userId)
              .concat(req.user.id);
            const allUsers = await r
              .table("members")
              .getAll(req.channel.guildId, {index: "guildId"})
              .eqJoin("userId", r.table("users"))
              .zip()
              .filter(function(doc) {
                return r.and([
                  doc("presence").ne(constants.presence.dnd),
                  r
                    .expr(notMentioningUsers)
                    .contains(doc("userId"))
                    .not()
                ]);
              })
              .map(function(doc) {
                return doc("userId");
              })
              .run(r.conn);

            return utils.cursorIterator(allUsers);
          } else {
            const matches = pushBody.data.content.match(/<@(\d)+>/g);
            if (matches && matches.length) {
              console.log("matches", matches, currentSubscriptions);
              const notMentioningUsers = new Set(
                Array.from(currentSubscriptions)
                  .map(sub => sub.userId)
                  .concat(req.user.id)
              );
              const mentioningUsers = new Set();
              for (const match of matches) {
                const userId = match.slice(2, -1);
                if (!notMentioningUsers.has(userId)) {
                  mentioningUsers.add(userId);
                }
              }
              if (mentioningUsers.size) {
                console.log(mentioningUsers);
                return {
                  [Symbol.asyncIterator]() {
                    return {
                      users: Array.from(mentioningUsers),
                      async next() {
                        let userId = null;
                        while ((userId = this.users.pop())) {
                          const exists = await r
                            .table("members")
                            .get(userId + "-" + req.channel.guildId)
                            .do(doc =>
                              r.and([
                                doc.ne(null),
                                r
                                  .table("users")
                                  .get(userId)("presence")
                                  .ne(constants.presence.dnd)
                              ])
                            )
                            .run(r.conn);
                          if (exists) {
                            console.log("Iterated a user", userId);
                            return {
                              value: userId,
                              done: false
                            };
                          }
                        }
                        console.log("No more users in fake iterator");
                        return {
                          done: true
                        };
                      }
                    };
                  }
                };
                const mentioningCursor = await r
                  .table("members")
                  .getAll(Array.from(mentioningUsers), {index: "id"})

                  .filter(doc =>
                    r.and([
                      doc.ne(null),
                      r
                        .table("users")
                        .get(doc("userId"))
                        .ne(constants.presence.dnd)
                    ])
                  )
                  .map(doc => doc("userId"))
                  .run(r.conn);
                console.log(
                  "mentioning",
                  await mentioningCursor.toArray(),

                  await (await r
                    .table("members")
                    .getAll(Array.from(mentioningUsers), {
                      index: "id"
                    })
                    .run(r.conn)).toArray()
                );
                return utils.cursorIterator(mentioningCursor);
              }
            }
          }
          return null;
        },
        pushBody: {
          type: "MESSAGE",
          data: {
            mentionsEveryone:
              mentionsEveryone ||
              // Unnecessary at that point
              undefined,
            guildId: req.guild.id,
            channelId: req.channel.channelId,
            authorAvatarURL: `/avatars/${req.user.id}/${req.user.avatar}`,
            channelName: req.channel.name,
            authorName: req.member.nickname || req.user.username,
            guildName: req.guild.name,
            url: `/guilds/${req.guild.id}/${req.channel.id}/${messageId}`,
            content: messageFormat.content
          }
        }
      }
    );

    res.json({...messageFormat, nonce: req.body.nonce || undefined});
  }
);

router.use(
  "/messages/:messageId",
  permissionCheck(["VIEW_CHANNEL"]),
  async function(req, res, next) {
    const message = await r
      .table("messages")
      .get(req.params.messageId)
      .run(r.conn);
    if (!message) {
      return res.status(404).json({
        message: "Message not found"
      });
    }
    req.message = message;
    return next();
  },
  messages
);

router.patch("/", permissionCheck(["MANAGE_CHANNELS"]), async function(
  req,
  res
) {
  await r
    .table("channels")
    .update({
      id: req.channel.id,
      ...req.body
    })
    .run(r.conn);

  await utils.dispatch("CHANNEL_UPDATE", {
    guildId: req.guild.id,
    id: req.channel.id,
    ...req.body
  });

  return res.json({success: true});
});

const shortid = require("shortid");

router.post("/invites", permissionCheck(["INVITE"]), async function(req, res) {
  const shortcode = shortid.generate();
  await r
    .table("invites")
    .insert({
      channelId: req.channel.id,
      guildId: req.channel.guildId,
      id: shortcode
    })
    .run(r.conn);
  res.json({
    code: shortcode
  });
});

router.delete("/", permissionCheck(["MANAGE_CHANNELS"]), async function(
  req,
  res
) {
  if (req.channel.id == req.channel.guildId)
    return res.status(422).json({
      message: "Cannot currently delete the primary channel!"
    });
  await r
    .table("channels")
    .get(req.channel.id)
    .delete()
    .run(r.conn);
});

module.exports = router;
