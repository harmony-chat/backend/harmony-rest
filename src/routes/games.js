const express = require("express");
const router = express.Router();

const utils = require("../utils");
const r = require("../rethink");
const format = require("../format");

const gameRouter = require("./game");
const {checkAuth} = require("./api");

router.get("/search", async function(req, res) {
  const query = req.query.q;
  const gameCursor = await r
    .table("games")
    .between(r.minval, req.query.after || r.maxval, {
      index: "id",
      leftBound: "open"
    })
    .orderBy({index: r.asc("id")})
    .filter(r.row("name").match(query))
    .limit(50)
    .without("code")
    .run(r.conn);

  const games = [];
  for await (const game of utils.cursorIterator(gameCursor)) {
    games.push(format.game(game));
  }

  return res.json(games);
});

router.use(
  "/:gameId",
  async function(req, res, next) {
    req.game = await r
      .table("games")
      .get(req.params.gameId)
      .run(r.conn);
    if (!req.game)
      return res.status(404).json({message: "Game does not exist"});
    console.log("GAME!");

    next();
  },
  gameRouter
);

router.use(checkAuth);

router.put("/", async function(req, res) {
  const game = req.body;

  await r
    .table("games")
    .insert(game)
    .run(r.conn);

  res.json(format.game(game));
});

module.exports = router;
