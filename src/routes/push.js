const router = require("express").Router();
const deviceRouter = require("./devices");
const utils = require("../utils");
const r = require("../rethink");
const format = require("../format");
const crypto = require("crypto");
const {checkAuth} = require("./api");
const {checkLogin} = require("../token");

router.patch("/subscription", async function(req, res) {
  // TODO: Actually verify lole
  const key = req.body.key;
  // await crypto.subtle.digest("SHA-512", publicKeyRaw)
  const deviceId = crypto
    .createHash("sha512")
    .update(Buffer.from(req.body.key, "base64"))
    // Rethink has limits of 127 char primary keys which is 1 under the hex representation. amazing.
    .digest("base64");

  const subscriptionData = req.body.data;

  const authToken = req.headers.authorization;
  if (authToken) {
    const userId = authToken
      ? authToken.substring(0, authToken.indexOf("."))
      : null;
    const user = await r
      .table("users")
      .get(userId)
      .run(r.conn);
    if (user && checkLogin(authToken, user.passwordHash, userId)) {
      req.user = user;
    }
  }

  await r
    .table("devices")
    .get(deviceId)
    .replace({
      id: deviceId,
      ownerId: req.user ? req.user.id : null,
      subscriptionData,
      enabled: subscriptionData !== null && req.user != undefined
    })
    .run(r.conn);

  await res.status(204).send("");
});

router.use(checkAuth);

router.get("/devices", async function(req, res) {
  const deviceCursor = await r
    .table("devices")
    .getAll(req.user.id, {index: "ownerId"})
    .run(r.conn);
  const devices = [];
  for await (const device of utils.cursorIterator(deviceCursor)) {
    devices.push(format.device(device));
  }
  res.json(devices);
});

router.use(
  "/devices/:deviceId",
  async function(req, res, next) {
    req.device = await r
      .table("devices")
      .get(req.params.deviceId)
      .run(r.conn);
    console.log("devices fetching", req.params.deviceId, req.device);
    if (!req.device) return res.status(404).json(null);
    if (req.device.ownerId != req.user.id) return res.status(404).json(null);
    next();
  },
  deviceRouter
);

module.exports = router;
