const router = require("express").Router();
const env = require("../env");
const r = require("../rethink");
const sharp = require("sharp");
const minio = require("../minio");
const format = require("../format");
const utils = require("../utils");

router.patch("/", async function(req, res) {
  const newUser = {
    ...req.body,
    id: req.user.id
  };
  await r
    .table("users")
    .update(newUser)
    .run(r.conn);

  const formatted = format.user({
    ...req.user,
    ...newUser
  });

  const guildCursor = await r
    .table("members")
    .getAll(req.user.id, {index: "userId"})
    .run(r.conn);
  const guilds = [];
  try {
    while ((nextGuild = await guildCursor.next())) {
      guilds.push(nextGuild.guildId);
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }

  await utils.dispatch("USER_UPDATE", {
    ...formatted,
    userId: req.user.id
  });

  return res.json(formatted);
});

router.get("/styles", async function(req, res) {
  const style = await r
    .table("styles")
    .get(req.user.id)
    .run(r.conn);

  res.json({
    style: (style && style.value) || ""
  });
});

router.patch("/styles", async function(req, res) {
  await r
    .table("styles")
    .get(req.user.id)
    .replace({
      id: req.user.id,
      value: req.body.style
    })
    .run(r.conn);

  return res.status(204).send();
});

router.put("/avatar", async function(req, res) {
  const avatarId = await utils.generateSnowflake();
  await minio.putObject(
    env.minio.bucketPrefix + "-user-avatars",
    // Maybe we should make this a hash..?
    avatarId,
    req.pipe(
      sharp()
        .resize({
          width: 100,
          height: 100,
          fit: "cover",
          background: {r: 0, g: 0, b: 0, alpha: 0}
        })
        .png()
    )
  );

  await r
    .table("users")
    .update({
      id: req.user.id,
      // Again, removing??
      avatar: avatarId
    })
    .run(r.conn);

  const formatted = format.user({
    ...req.user,
    // TODO: Removing icon?
    avatar: avatarId
  });

  const guildCursor = await r
    .table("members")
    .getAll(req.user.id, {index: "userId"})
    .run(r.conn);
  const guilds = [];
  try {
    while ((nextGuild = await guildCursor.next())) {
      guilds.push(nextGuild.guildId);
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }

  await utils.dispatch("USER_UPDATE", {
    ...formatted,
    guildId: guilds
  });

  console.log("...", formatted);

  res.json(formatted);
});

module.exports = router;
