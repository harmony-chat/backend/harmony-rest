const router = require("express").Router();
const permissionCheck = require("../permissionCheck");
const format = require("../format");
const r = require("../rethink");
const utils = require("../utils");

router.get("/", function(req, res) {
  if (req.role.id == req.guild.id) req.role.position = Number.MAX_SAFE_INTEGER;
  return res.json(format.role(req.role));
});

router.delete("/", permissionCheck(["MANAGE_ROLES"]), async function(req, res) {
  await r
    .table("roles")
    .get(req.role.id)
    .delete()
    .run(r.conn);
  await r
    .table("roles")
    .between(
      [req.role.position, req.role.guildId],
      [Number.MAX_SAFE_INTEGER, req.role.guildId],
      {
        leftBound: "closed",
        rightBound: "open"
      }
    )
    .update({position: r.row("position").add(-1)})
    .run(r.conn);
  await utils.dispatch("ROLE_DELETE", {
    guildId: req.guild.id,
    id: req.role.id
  });
  res.json({success: true});
});

router.patch("/", permissionCheck(["MANAGE_ROLES"]), async function(req, res) {
  if (req.body.position && req.body.position != req.role.position) {
    if (req.role.id == req.guild.id)
      return res
        .status(422)
        .json({message: "Cannot change the position of the default role"});
    const negative = req.body.position < req.role.position;
    const min = negative ? req.body.position : req.role.position;
    const max = negative ? req.role.position : req.body.position;

    const roles = await r
      .table("roles")
      .between([min, req.role.guildId], [max, req.role.guildId], {
        index: "position_guildId",
        leftBound: negative ? "closed" : "open",
        rightBound: negative ? "open" : "closed"
      })
      .update({
        position: r.row("position").add(negative ? 1 : -1)
      })
      .run(r.conn);
  }
  await r
    .table("roles")
    .update({
      ...req.body,
      id: req.role.id
    })
    .run(r.conn);
  const newRole = {
    ...req.role,
    ...req.body,
    id: req.role.id
  };

  await utils.dispatch(
    "ROLE_UPDATE",
    format.role({
      ...req.body,
      id: req.role.id,
      oldPosition:
        req.body.position !== undefined &&
        req.body.position != req.role.position
          ? req.role.position
          : undefined,
      guildId: req.role.guildId
    })
  );

  res.json(format.role(newRole));
});

module.exports = router;
