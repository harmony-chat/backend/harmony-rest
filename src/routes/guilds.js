const router = require("express").Router();
const format = require("../format");
const r = require("../rethink");
const utils = require("../utils");
const permissionCheck = require("../permissionCheck");
const roleRouter = require("./roles");
const sanitizeCSS = require("../sanitizeCSS");
const css = require("css");

router.get("/", function(req, res) {
  res.json(format.guild(req.guild));
});

router.patch("/styles", permissionCheck(["MANAGE_GUILD"]), async function(
  req,
  res
) {
  try {
    var ast = css.parse(req.body.localStyle);
  } catch (err) {
    // Should be in a schema
    return res.status(422).json({
      message: "Invalid CSS"
    });
  }

  try {
    ast.stylesheet.rules = sanitizeCSS.cleanRules(ast.stylesheet.rules);
  } catch (err) {
    console.log(err);
    return res.status(422).json({
      message: "Your styles are evil"
    });
  }

  const cleanedStyle = css.stringify(ast, {compress: true});

  await r
    .table("styles")
    .get(req.guild.id)
    .replace({
      id: req.guild.id,
      value: cleanedStyle
    })
    .run(r.conn);

  console.log("About to dispatch!");
  await utils.dispatch("GUILD_STYLE_UPDATE", {
    localStyle: cleanedStyle,
    guildId: req.guild.id,
    id: req.guild.id
  });
  console.log("done, responding");

  res.json({id: req.guild.id, localStyle: cleanedStyle});
});

router.get("/styles", async function(req, res) {
  const localStyle = await r
    .table("styles")
    .get(req.guild.id)
    .run(r.conn);

  // We won't send an error because it's fine
  if (!localStyle) return res.json({id: req.guild.id, localStyle: ""});

  return res.json({id: req.guild.id, localStyle: localStyle.value});
});

router.patch("/", permissionCheck(["MANAGE_GUILD"]), async function(req, res) {
  await r
    .table("guilds")
    .update({
      ...req.body,
      id: req.guild.id
    })
    .run(r.conn);

  await utils.dispatch("GUILD_UPDATE", {
    ...req.body,
    guildId: req.guild.id,
    id: req.guild.id
  });

  return res.json({success: true});
});

router.post("/roles", permissionCheck(["MANAGE_ROLES"]), async function(
  req,
  res
) {
  const roleCount = await r
    // Dude I don't know
    .table("roles")
    .getAll(req.guild.id, {index: "guildId"})
    .filter(r.row("id").ne(req.guild.id))
    .count()
    .run(r.conn);

  const roleData = {
    guildId: req.guild.id,
    color: req.body.color || 16777215,
    name: req.body.name || "New Role",
    id: await utils.generateSnowflake(),
    position: roleCount
  };
  const role = await r
    .table("roles")
    .insert(roleData)
    .run(r.conn);

  const formatted = format.role(roleData);
  await utils.dispatch("ROLE_CREATE", formatted);

  return res.json(formatted);
});

router.get("/roles", async function(req, res) {
  const roleCursor = await r
    .table("roles")
    .getAll(req.guild.id, {index: "guildId"})
    .run(r.conn);
  const roles = [];
  try {
    while ((nextRole = await roleCursor.next())) {
      roles.push(format.role(nextRole));
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }
  return res.json(roles);
});

router.use(
  "/roles/:roleId",
  async function(req, res, next) {
    const role = await r
      .table("roles")
      .get(req.params.roleId)
      .run(r.conn);

    if (!role || role.guildId != req.guild.id) {
      return res.status(404).json({message: "Role not found"});
    }
    req.role = role;
    next();
  },
  roleRouter
);

router.get("/channels", async function(req, res) {
  const channelCursor = await r
    .table("channels")
    .getAll(req.guild.id, {index: "guildId"})
    .run(r.conn);
  const channels = [];
  try {
    while ((nextChannel = await channelCursor.next())) {
      channels.push(format.channel(nextChannel));
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }
  return res.json(channels);
});

const minio = require("../minio");
const env = require("../env");

const sharp = require("sharp");

router.put("/icon", permissionCheck(["MANAGE_GUILD"]), async function(
  req,
  res
) {
  await minio.putObject(
    env.minio.bucketPrefix + "-guild-icons",
    // Maybe we should make this a hash..?
    req.guild.id,
    req.pipe(
      sharp()
        .resize({
          width: 128,
          height: 128,
          fit: "cover",
          background: {r: 0, g: 0, b: 0, alpha: 0}
        })
        .png()
    )
  );

  await r
    .table("guilds")
    .update({
      id: req.guild.id,
      // Again, removing??
      icon: true
    })
    .run(r.conn);

  const formatted = format.guild({
    ...req.guild,
    // TODO: Removing icon?
    icon: true
  });

  console.log("...", formatted);

  utils.dispatch("GUILD_UPDATE", {
    ...formatted,
    guildId: req.guild.id
  });

  res.json(formatted);
});

router.put("/channels", permissionCheck(["MANAGE_CHANNELS"]), async function(
  req,
  res
) {
  const channelObject = {
    name: req.body.name,
    id: await utils.generateSnowflake(),
    guildId: req.guild.id,
    media: !!req.body.media
  };

  await r
    .table("channels")
    .insert(channelObject)
    .run(r.conn);

  const formattedChannel = format.channel(channelObject);
  utils.dispatch("CHANNEL_CREATE", formattedChannel);
  return res.json(formattedChannel);
});

router.post("/leave", async function(req, res) {
  await r
    .table("members")
    .get(req.member.id)
    .delete()
    .run(r.conn);
  utils.dispatch("GUILD_DELETE", {
    userId: req.user.id,
    id: req.guild.id,
    guildId: req.guild.id
  });
  res.json({success: true});
});

const memberRouter = require("./members");

router.use(
  "/members/:memberId",
  async function(req, res, next) {
    req.requestMember = await r
      .table("members")
      .get(`${req.params.memberId}-${req.guild.id}`)
      .run(r.conn);
    next();
  },
  memberRouter
);

module.exports = router;
