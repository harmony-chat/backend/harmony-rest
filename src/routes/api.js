const router = require("express").Router();
const r = require("../rethink");
const {generateMagic, generateToken, checkLogin} = require("../token");

module.exports = router;

async function checkAuth(req, res, next) {
  const authToken = req.headers.authorization;
  if (!authToken)
    return res.status(403).json({
      message: "No Authorization Header Provided"
    });

  const userId = authToken.substring(0, authToken.indexOf("."));

  const user = await r
    .table("users")
    .get(userId)
    .run(r.conn);

  if (!user)
    return res.status(403).json({
      message: "Invalid token."
    });

  if (!checkLogin(authToken, user.passwordHash, userId))
    return res.status(403).json({
      message: "Invalid token."
    });
  console.log("login checksy");

  req.user = user;

  console.log("set, nexting");
  return next();
}

module.exports.checkAuth = checkAuth;

const roleFetcher = require("../roleFetcher");
const channelRouter = require("./channels");
const guildRouter = require("./guilds");
const userRouter = require("./users");
const selfRouter = require("./self");
const inviteRouter = require("./invites");
const pushRouter = require("./push");
const gameRouter = require("./games");
const bcrypt = require("bcrypt");
const utils = require("../utils");
const format = require("../format");
const env = require("../env");

const minio = require("../minio");

router.use(utils.validateSchema.bind(null, router));

router.use("/games", gameRouter);

router.get("/icons/:guildId", async function(req, res) {
  const guild = await r
    .table("guilds")
    .get(req.params.guildId)
    .run(r.conn);
  if (!guild || !guild.icon)
    return res.status(404).json({message: "Not found"});

  const stream = await minio.getObject(
    env.minio.bucketPrefix + "-guild-icons",
    guild.id
  );
  stream.on("error", function(err) {
    console.error("Error fetching uploaded icon!", guild.id, err);
    res.status(500).json({message: "Internal error occurred"});
  });
  stream.pipe(res);
});

router.get("/attachments/:attachmentId", async function(req, res) {
  const attachment = await r
    .table("attachments")
    .get(req.params.attachmentId)
    .run(r.conn);
  if (!attachment || !attachment.uploaded)
    return res.status(404).json({message: "Not found"});

  const stream = await minio.getObject(
    env.minio.bucketPrefix + "-message-attachments",
    attachment.id
  );
  stream.on("error", function(err) {
    console.error("Error fetching uploaded attachment!", attachment.id, err);
    res.status(500).json({message: "Internal error occurred"});
  });
  stream.pipe(res);
});

router.get("/avatars/:userId/:avatarId", async function(req, res) {
  // Prevents us from fetching a stale avatar
  // TODO: Actually make a proper fetching thing for the avatarId rather than just a pre-shared filename for caching purposes
  const user = await r
    .table("users")
    .get(req.params.userId)
    .run(r.conn);

  if (!user || !user.avatar)
    return res.status(404).json({message: "Not found"});

  const stream = await minio.getObject(
    env.minio.bucketPrefix + "-user-avatars",
    user.avatar
  );
  stream.on("error", function(err) {
    console.error("Error fetching uploaded avatar!", avatar.id, err);
    res.status(500).json({message: "Internal error occurred"});
  });
  stream.pipe(res);
});

router.post("/login", async function(req, res) {
  console.log("login");
  const userCursor = await r
    .table("users")
    .getAll(req.body.email, {index: "email"})
    .limit(1)
    .run(r.conn);
  const users = await userCursor.toArray();
  console.log("userCursor", users);
  if (users.length === 0) {
    console.log("users.length=0");
    return res.status(403).json({
      message: "Invalid login"
    });
  }

  const user = users[0];

  console.log(user);
  console.log("ack");
  const matchingPassword = await bcrypt.compare(
    req.body.password,
    user.passwordHash
  );
  console.log("checkowo", matchingPassword);
  if (!matchingPassword)
    return res.status(403).json({
      message: "Invalid login"
    });

  return res.json({
    token: generateToken(user.id, user.passwordHash)
  });
});

router.post("/register", async function(req, res) {
  console.log(req.body, "BOD0!");
  await r
    .table("users")
    .insert({
      email: req.body.email,
      username: req.body.username,
      id: await utils.generateSnowflake(),
      passwordHash: await bcrypt.hash(req.body.password, 10)
    })
    .run(r.conn);
  return res.json({success: true});
});

router.get("/__debug_snowflake", async function(req, res) {
  const id = await utils.generateSnowflake();
  return res.json(id);
});

router.get("/gateway", function(req, res) {
  console.log(req.headers.host);
  res.json({
    url:
      // can i interest you in some high quality convenience/debug code
      req.headers.host == "localhost:3000"
        ? env.gateway
        : `wss://${req.headers.host}/ws`
  });
});

router.use("/push", pushRouter);

router.use(checkAuth);
router.use(
  "/channels/:channelId",
  async function channelPopulator(req, res, next) {
    const channel = await r
      .table("channels")
      .get(req.params.channelId)
      .run(r.conn);
    if (!channel) {
      return res.status(404).json({
        message: "Channel not found"
      });
    }

    const member = await r
      .table("members")
      .get(req.user.id + "-" + channel.guildId)
      .run(r.conn);
    if (!member) {
      return res.status(404).json({
        message: "Channel not found"
      });
    }

    const guild = await r
      .table("guilds")
      .get(channel.guildId)
      .run(r.conn);
    if (!guild) {
      return res.status(404).json({message: "Channel not found"});
    }

    req.guild = guild;
    req.channel = channel;
    req.member = member;
    return next();
  },
  roleFetcher,
  channelRouter
);

router.use(
  "/guilds/:guildId",
  async function guildPopulator(req, res, next) {
    const guild = await r
      .table("guilds")
      .get(req.params.guildId)
      .run(r.conn);
    if (!guild) {
      console.log("no guild");
      return res.status(404).json({
        message: "Guild not found"
      });
    }
    const member = await r
      .table("members")
      .get(req.user.id + "-" + guild.id)
      .run(r.conn);
    if (!member) {
      console.log("not member");
      return res.status(404).json({message: "Guild not found"});
    }
    req.guild = guild;
    req.member = member;
    return next();
  },
  roleFetcher,
  guildRouter
);

router.use(
  "/users/:userId",
  async function userPopulator(req, res, next) {
    const user = await r
      .table("users")
      .get(req.params.userId)
      .run(r.conn);
    if (!user) {
      return res.status(404).json({
        message: "User not found"
      });
    }
    req.user = user;
    return next();
  },
  userRouter
);

router.use(
  "/invites/:inviteId",
  async function invitePopulator(req, res, next) {
    const invite = await r
      .table("invites")
      .get(req.params.inviteId)
      .run(r.conn);
    if (!invite) return res.status(404).json({error: "Invite not found"});
    const guild = await r
      .table("guilds")
      .get(invite.guildId)
      .run(r.conn);
    // Just in case....
    if (!guild) return res.status(404).json({error: "Invite not found"});
    req.invite = invite;
    req.guild = guild;
    return next();
  },
  inviteRouter
);

router.put("/guilds", async function(req, res) {
  const guildId = await utils.generateSnowflake();
  const guildObject = {
    name: req.body.name,
    id: guildId,
    ownerId: req.user.id
  };
  await r
    .table("guilds")
    .insert(guildObject)
    .run(r.conn);
  await r
    .table("roles")
    .insert({
      name: "Members",
      id: guildId,
      guildId,
      position: Number.MAX_SAFE_INTEGER,
      permissions: env.defaultPermissions
    })
    .run(r.conn);
  await r
    .table("members")
    .insert({
      id: `${req.user.id}-${guildId}`,
      guildId,
      userId: req.user.id
    })
    .run(r.conn);
  const channelObject = {
    id: guildId,
    name: "general",
    guildId
  };
  await r
    .table("channels")
    .insert(channelObject)
    .run(r.conn);
  const formattedGuild = format.guild(guildObject);
  await utils.dispatch("GUILD_CREATE", {
    ...formattedGuild,
    userId: req.user.id,
    guildId: guildId,
    channelId: guildId
  });
  const formattedChannel = format.channel(channelObject);
  await utils.dispatch("CHANNEL_CREATE", formattedChannel);
  return res.json({g: formattedGuild, c: formattedChannel});
});

router.get("/guilds", async function(req, res) {
  const guildCursor = await r
    .table("members")
    .getAll(req.user.id, {index: "userId"})
    .slice(req.query.after || 0, (req.query.after || 0) + 100)
    .eqJoin("guildId", r.table("guilds"))
    .run(r.conn);
  const guilds = [];
  try {
    while ((nextGuild = await guildCursor.next())) {
      guilds.push(format.guild(nextGuild.right));
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }

  res.json(guilds);
});

router.use("/self", selfRouter);
