const router = require("express").Router();
const format = require("../format");
const utils = require("../utils");
const r = require("../rethink");

router.get("/", function(req, res) {
  return res.json(format.invite(req.guild, req.invite));
});

router.post("/", async function(req, res) {
  await r
    .table("members")
    .insert({
      id: `${req.user.id}-${req.guild.id}`,
      guildId: req.guild.id,
      userId: req.user.id
    })
    .run(r.conn);
  const channel = await r
    .table("channels")
    .get(req.invite.channelId || req.invite.guildId)
    .run(r.conn);
  const guildFormatted = format.guild(req.guild);
  const channelFormatted = format.channel(channel);
  utils.dispatch("GUILD_CREATE", {
    userId: req.user.id,
    ...guildFormatted
  });
  return res.json({
    g: guildFormatted,
    c: channelFormatted
  });
});

module.exports = router;
