const format = require("../format");

const express = require("express");
const router = express.Router();
const minio = require("../minio");
const env = require("../env");

const {checkAuth} = require("./api");

router.get("/", function(req, res) {
  console.log("fetched game");
  res.json(format.game(req.game));
});

router.get("/assets/:filename", async function(req, res) {
  const stream = await minio.getObject(
    env.minio.bucketPrefix + "-game-assets",
    req.game.id + "_" + req.params.filename
  );

  stream.on("error", function(err) {
    console.error(
      "Error fetching uploaded asset!",
      req.params.filename,
      req.game.id,
      err
    );
    res.status(500).json({message: "Internal error occurred"});
  });

  stream.pipe(res);
});

router.use(checkAuth, async function(req, res, next) {
  console.log("Checking for ownership...");
  if (req.game.ownerId != req.user.id) {
    return res.status(403).json({
      message: "You don't own that game!"
    });
  }

  next();
});

router.patch("/", async function(req, res) {
  await r
    .table("games")
    .get(req.game.id)
    .update(format.game(req.body));
  return res.status(204).send();
});

router.put("/assets/:filename", async function(req, res) {
  await minio.putObject(
    env.minio.bucketPrefix + "-game-assets",
    // Maybe we should make this a hash..?
    req.game.id + "_" + req.params.filename,
    req
  );

  res.status(204).send();
});

module.exports = router;
