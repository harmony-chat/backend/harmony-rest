const router = require("express").Router();
const format = require("../format");
const r = require("../rethink");

router.get("/", function(req, res) {
  res.json(format.user(req.user));
});

// router.get("/channels", async function(req, res) {
//   const channelCursor = await r
//     .table("channels")
//     .getAll(req.guild.id, {index: "guildId"})
//     .run(r.conn);
//   const channels = [];
//   try {
//     while ((nextChannel = await channelCursor.next())) {
//       channels.push(format.channel(nextChannel));
//     }
//   } catch (err) {
//     if (!err || err.msg != "No more rows in the cursor.") throw err;
//   }
//   return res.json(channels);
// });

module.exports = router;
