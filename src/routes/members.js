const router = require("express").Router();
const permissionCheck = require("../permissionCheck");
const format = require("../format");
const utils = require("../utils");
const r = require("../rethink");

router.patch("/", async function(req, res) {
  if (
    !permissionCheck.rawPermissionCheck(req.totalPermissions, [
      "MANAGE_NICKAMES"
    ])
  ) {
    if (req.member.id != req.requestMember.id) {
      return res.status(403).json({
        permissionsNeeded: ["MANAGE_NICKAMES"]
      });
    }
    if (
      req.member.id == req.requestMember.id &&
      !permissionCheck.rawPermissionCheck(req.totalPermissions, [
        "SET_NICKNAME"
      ])
    ) {
      return res.status(403).json({
        permissionsNeeded: ["SET_NICKNAME"]
      });
    }
  }
  await r
    .table("members")
    .update({
      ...req.body,
      id: req.requestMember.id
    })
    .run(r.conn);

  const formatted = format.member({
    ...req.requestMember,
    ...req.body
  });

  utils.dispatch("MEMBER_UPDATE", formatted);

  return res.json(formatted);
});

router.get("/roles", async function(req, res) {
  const roleCursor = await r
    .table("role_members")
    .getAll([req.requestMember.userId, req.guild.id], {
      index: "memberId_guildId"
    })
    .run(r.conn);
  const roles = [];
  try {
    while ((nextRole = await roleCursor.next())) {
      roles.push(nextRole.roleId);
    }
  } catch (err) {
    if (!err || err.msg != "No more rows in the cursor.") throw err;
  }
  return res.json(roles);
});

router.use("/roles/:roleId", async function(req, res, next) {
  const role = await r
    .table("roles")
    .get(req.params.roleId)
    .run(r.conn);
  if (!role || role.guildId != req.guild.id)
    return res.status(404).json({message: "Role not found"});
  req.role = role;
  next();
});

router.delete(
  "/roles/:roleId",
  permissionCheck(["MANAGE_ROLES"]),
  async function(req, res) {
    const response = await r
      .table("role_members")
      .get(`${req.requestMember.userId}-${req.role.id}-${req.guild.id}`)
      .delete()
      .run(r.conn);

    if (response.deleted) {
      await utils.dispatch("MEMBER_ROLES_DELETE", {
        guildId: req.guild.id,
        memberId: req.requestMember.userId,
        roleId: req.role.id
      });
      return res.status(204).send();
    } else {
      return res.status(404).json({message: "Member wasn't in that role"});
    }
  }
);

router.put("/roles/:roleId", permissionCheck(["MANAGE_ROLES"]), async function(
  req,
  res
) {
  try {
    await r
      .table("role_members")
      .insert({
        guildId: req.guild.id,
        memberId: req.requestMember.userId,
        roleId: req.role.id,
        id: `${req.requestMember.userId}-${req.role.id}-${req.guild.id}`
      })
      .run(r.conn);
  } catch (err) {
    console.error(err);
    if (err.message == "Duplicate primary key `id`") {
      return res.status(404).json({message: "Member is already in that role"});
    } else throw err;
  }
  console.log("roles_add");

  await utils.dispatch("MEMBER_ROLES_ADD", {
    guildId: req.guild.id,
    memberId: req.requestMember.userId,
    roleId: req.role.id
  });

  res.status(204).send();
});

module.exports = router;
