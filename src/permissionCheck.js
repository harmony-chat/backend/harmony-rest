const permissions = {
  INVITE: 1 << 0,
  KICK: 1 << 1,
  BAN: 1 << 2,
  ADMIN: 1 << 3,
  MANAGE_CHANNELS: 1 << 4,

  VIEW_CHANNEL: 1 << 10,
  MESSAGE_CREATE: 1 << 11,
  MANAGE_MESSAGES: 1 << 13,

  MANAGE_NICKNAMES: 1 << 14,
  MANAGE_ROLES: 1 << 15,
  MANAGE_GUILD: 1 << 16,
  SET_NICKNAME: 1 << 17,
  MENTION_EVERYONE: 1 << 18
};

function rawPermissionCheck(calculatedPermissions, totalNeededPerms) {
  if (totalNeededPerms[0] instanceof String)
    totalNeededPerms = permissionsNeeded.map(function(permName) {
      if (permissions[permName] === undefined)
        throw new Error("Invalid permission bit: " + permName);
      return permissions[permName];
    });
  return !(
    (calculatedPermissions & permissions.ADMIN) !== permissions.ADMIN &&
    totalNeededPerms.some(
      permissionBit => (calculatedPermissions & permissionBit) !== permissionBit
    )
  );
}

module.exports = function permissionCheckGenerator(permissionsNeeded) {
  const totalNeededPerms = permissionsNeeded.map(function(permName) {
    if (permissions[permName] === undefined)
      throw new Error("Invalid permission bit: " + permName);
    return permissions[permName];
  });

  return function permissionCheck(req, res, next) {
    console.log("perm checks", req.totalPermissions, totalNeededPerms);
    // TODO: Permissions system
    if (!rawPermissionCheck(req.totalPermissions, totalNeededPerms)) {
      console.log("Missing permissions", res._json);
      return res.status(403)._json({
        permissionsNeeded
      });
    }
    console.log("h");
    next();
  };
};

module.exports.permissions = permissions;
module.exports.rawPermissionCheck = rawPermissionCheck;
