const express = require("express");
const app = express();
const apiRouter = require("./src/routes/api");
const bodyParser = require("body-parser");
const cors = require("cors");

const server = app.listen(3000, function() {
  app.use("/api", cors(), express.json(), apiRouter);
});
